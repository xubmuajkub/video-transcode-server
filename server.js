const express = require('express')
const cors = require('cors')
const port = 3000
const upload = require('./upload')
const env = require('./env')
const path = require('path')
const queue = require('./queue')
const crypto = require('crypto')
const logger = require('morgan')

const app = express()
app.use(cors())
app.use(logger('dev'))

app.get('/', async (req, res) => {
  return res.json({
    message: 'Hello world'
  })
})

app.post('/upload', upload.fields([{ name: 'file', maxCount: 1 }, { name: 'subtitle', maxCount: 1 }]), async (req, res) => {
  try {
    const { file, subtitle } = req.files
    const prefix = req.body.prefix ? req.body.prefix + '-' : ''
    const subtitleDelay = req.body.subtitleDelay || 0
    const output = `${process.env.NODE_ENV === 'development' ? 'test-' : ''}${prefix}${crypto.randomBytes(12).toString('hex')}${Date.now()}`

    const outputProcess = path.join(__dirname, env.uploadStorage, 'outputProcess_' + path.basename(file[0].filename))
    queue.add({
      filepath: file[0].path,
      outputProcess: outputProcess,
      subtitle: subtitle && subtitle[0].path,
      output: path.join(env.outputStorage, output),
      subtitleDelay: parseFloat(subtitleDelay)
    })
  
    return res.json({
      success: true,
      movieId: output,
      movieName: file[0].originalname,
      subtitleDelay,
      link: `https://${env.storageAccount}.blob.core.windows.net/${output}/playlist.m3u8`
    })
  } catch (error) {
    console.log(error)
    return res.status(500).send({
      message: 'Server error'
    })
  }
})

app.get('/jobs', async (req, res) => {
  try {
    const jobs = await queue.getJobCounts()

    return res.json(jobs)
  } catch (error) {
    return res.status(500).send({
      message: 'Server error'
    })
  }
})

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))