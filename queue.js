const Queue = require('bull')
const hls = require('./hls')
const env = require('./env')
const azure = require('azure-storage')
const fs = require('fs-extra')
const path = require('path')
const util = require('util')

const blobService = azure.createBlobService(env.storageAccount, env.storageAccessKey)
const videoQueue = new Queue('video transcoding', `redis://${env.redisHost}:6379`)

const createContainerIfNotExistsPromise = util.promisify(blobService.createContainerIfNotExists.bind(blobService))
const createBlockBlobFromLocalFilePromise = util.promisify(blobService.createBlockBlobFromLocalFile.bind(blobService))

videoQueue.process(async (job) => {
  try {
    await hls.preprocess(job.data.filepath, job.data.outputProcess, job.data.subtitle, job.data.subtitleDelay)
    await hls.generateHLS(job.data.outputProcess, job.data.output)
    await fs.remove(job.data.filepath)
    await fs.remove(job.data.outputProcess)

    if (job.data.subtitle) {
      await fs.remove(job.data.subtitle)
    }
    return Promise.resolve({ output: job.data.output })
  } catch (error) {
    await fs.remove(job.data.filepath)
    await fs.remove(job.data.outputProcess)
    await fs.remove(job.data.output)
    if (job.data.subtitle) {
      await fs.remove(job.data.subtitle)
    }
    return Promise.reject(error)
  }
})

videoQueue.on('active', function(job, jobPromise){
  console.log(`JobID ${job.id} has started`)
})

videoQueue.on('completed', async (job, result) => {
  try {
    console.log(`Job ${job.id} completed, prepare to send to cloud storage`)
    const containerName = path.basename(result.output)

    // note: container has some rules to follow (try create one on Azure portal to see in error printed out)
    await createContainerIfNotExistsPromise(containerName, {
      publicAccessLevel: 'blob'
    })

    const files = fs.readdirSync(result.output)
    for (const file of files) {
      const filepath = path.join(__dirname, result.output, file)
      await createBlockBlobFromLocalFilePromise(containerName, path.basename(file), filepath)
    }

    console.log('All files uploaded to cloud storage')

    await fs.remove(result.output)
  } catch (error) {
    console.log(error)
  }
})

videoQueue.on('failed', function(job, err){
  console.log(`JobID ${job.id} Failed`)
  console.log(err)
})

videoQueue.on('removed', (job) => {
  // A job successfully removed.
  console.log(`JobID ${job.id} successfully removed`)
})

module.exports = videoQueue