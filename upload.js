const crypto = require('crypto')
const multer = require('multer')
const path = require('path')
const env = require('./env')

const options = {
  destination: env.uploadStorage,
  filename: (req, file, cb) => { // TODO: file type
    cb(null, `${crypto.randomBytes(12).toString('hex')}_${Date.now()}${path.extname(file.originalname)}`)
  },
}

const storage = multer.diskStorage(options)

const fileFilter = (req, file, cb) => {
  const types = ['.mp4', '.avi', '.mkv', '.mov', '.srt']
  if (!types.includes(path.extname(file.originalname))) {
    return req.res.status(422).send({
      message: `Only ${types.join(', ')} files are allowed!`
    })
  }
  cb(null, true)
}

module.exports = multer({ storage, fileFilter })
